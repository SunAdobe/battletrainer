﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Core {
  class Program {
    static void Main(String[] args) {
      FileInfo[] Files = new DirectoryInfo(String.Format(@"{0}", args[0])).GetFiles("*.jpg");
      foreach (FileInfo Current in Files) {
        Console.WriteLine(Current.FullName);
        Bitmap Source = new Bitmap(Current.FullName);
        Bitmap CroppedImage = Source.Clone(new System.Drawing.Rectangle(585, 172, 188, 275), Source.PixelFormat);
        CroppedImage.Save(Current.FullName + "_resized.jpg");
      }
    }
  }
}
