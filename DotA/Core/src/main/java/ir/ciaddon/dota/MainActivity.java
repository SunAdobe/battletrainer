package ir.ciaddon.dota;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import ir.ciaddon.dota.facilities.FontChangeCrawler;

public class MainActivity extends AppCompatActivity {

  @Override
  public void setContentView(View view) {
    super.setContentView(view);
    /*=-=-=-=-=-[  ]-=-=-=-=-=*/
    FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "fonts/IRANSans-web.ttf");
    fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content).getRootView());
    /*=-=-=-=-=-[  ]-=-=-=-=-=*/
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    /*=-=-=-=-=-[  ]-=-=-=-=-=*/
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
      WindowManager.LayoutParams.FLAG_FULLSCREEN);
    /*=-=-=-=-=-[  ]-=-=-=-=-=*/
    setContentView(R.layout.activity_main);
  }
}
